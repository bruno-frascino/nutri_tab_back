package io.octopustech.nutrimais.test.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;
import javax.ws.rs.NotFoundException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.octopustech.nutrimais.persistence.dal.MessageDAO;
import io.octopustech.nutrimais.persistence.model.Message;
import io.octopustech.nutrimais.service.MessageServiceImpl;
import io.octopustech.nutrimais.util.MessageKeys;
import io.octopustech.nutrimais.util.NutrimaisMessageBundle;


@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class MessageServiceTest {
	
	@Mock
	MessageDAO messageDAO;
	@Mock
	NutrimaisMessageBundle messageBundle;
	
	@InjectMocks
	MessageServiceImpl messageService;
	
	static Message msg1 = new Message();
	static Message msg2 = new Message();
	
	@BeforeClass
	public static void setUp(){
		msg1.setMessage("Msg1");
		msg2.setMessage("Msg2");
	}
	
	@Test
	public void listAllEmptyListTest(){
		
		//set expectations
		when(messageDAO.findAll(Message.class)).thenReturn(new ArrayList<>());
		
		//test
		List<Message> emptyList = messageService.listAll();
		
		//check method was called
		verify(messageDAO).findAll(Message.class);
		
		//assertions
		assertEquals(0, emptyList.size());
	}
	
	@Test
	public void listAllTest(){
		
		//Set return list
		List<Message> list = new ArrayList<>();
		list.add(msg1);
		list.add(msg2);
		
		//set expectations
		when(messageDAO.findAll(Message.class)).thenReturn(list);
		
		//test
		List<Message> listAll = messageService.listAll();
		
		//check method was called
		verify(messageDAO).findAll(Message.class);
		
		//assertions
		assertTrue(listAll.size() == 2);
	}

	 /** Only requirement for addition is that the new message 
	 * text cannot be empty or null
	 * When a messaged is added a create date is set
	 */
	@Test
	public void addValidMessageTest(){
		
		//set expectations
		when(messageDAO.save(msg1)).thenReturn(msg1);
		
		//test
		Message rMsg = messageService.add(msg1);
		
		//check method was called
		verify(messageDAO).save(msg1);
		
		//assertions
		assertEquals(LocalDate.now(), rMsg.getCreateDate());
	}
	
	@Test
	public void addEmptyMessageTest(){
				
		//test
		try{
			messageService.add(new Message());
			fail("A ValidationException should be thrown");
		}
		catch(ValidationException e){
			
			verify(messageBundle).getString(MessageKeys.MSG_EMPTY);
		}
	}
	
	@Test
	public void addNullMessageTest(){
		
		//test
		try{
			messageService.add(null);
			fail("A ValidationException should be thrown");
		}
		catch(ValidationException e){
			//method called
			verify(messageBundle).getString(MessageKeys.MSG_INVALID);
		}
	}
	
	
	/**
	 * Only requirement for update is that the new message 
	 * text cannot be empty or null
	 * When a message is updated a update date is updated
	 */
	@Test
	public void updateValidMessageTest(){
		
		//expectation
		Message newMsg = new Message();
		newMsg.setMessage("Hello");
		when(messageDAO.update(newMsg)).thenReturn(newMsg);
		
		//test
		Message rMsg = messageService.update(newMsg);
		
		//verify
		verify(messageDAO).update(newMsg);
		
		//assertions
		assertEquals(LocalDateTime.now(), rMsg.getUpdateDate());
	}
	
	
	@Test
	public void updateEmptyMessageTest(){
		//expectation
		Message newMsg = new Message();
		
		try{
			messageService.update(newMsg);
			fail("A ValidationException should be thrown");
		}
		catch(ValidationException e){

			verify(messageBundle).getString(MessageKeys.MSG_EMPTY);
		}
	}
	
	@Test
	public void updateNullMessageTest(){
		
		//test
		try{
			messageService.update(null);
			fail("A ValidationException should be thrown");
		}
		catch( ValidationException e){
			
			verify(messageBundle).getString(MessageKeys.MSG_INVALID);
		}
	}

	@Test
	public void removeValidMessageTest(){
		
		messageService.remove(123l);
		
		verify(messageDAO).remove(Message.class, 123l);
	}
	
	@Test
	public void removeInvalidArgumentTest(){
		
		try{
			messageService.remove(null);
			fail("A ValidationException should be thrown");
		}
		catch( ValidationException e){
			
			verify(messageBundle).getString(MessageKeys.ARG_INVALID);
		}
		
	}
	
	@Test(expected = NotFoundException.class )
	public void removeNoMessageTest(){
		
		doThrow(NotFoundException.class)
			.when(messageDAO)
			.remove(Message.class, -99l);
			
		messageService.remove(-99l);
	}
}
