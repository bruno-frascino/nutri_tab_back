package io.octopustech.nutrimais.persistence.dal;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.NotFoundException;

@Dependent
public class BasicDAOImpl<T> implements BasicDAO<T> {

	@Inject
	protected EntityManager entityManager;

	@Override
	public T save(T entity) {
		this.entityManager.persist(entity);
		return entity;
	}

	@Override
	public T findById(Class<T> clazz, Serializable id) {
		return this.entityManager.find(clazz, id);
	}

	@Override
	public List<T> findAll(Class<T> clazz) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(clazz);
		cq.from(clazz);
		return entityManager.createQuery(cq).getResultList();
	}

	@Override
	public T update(T entity) {
		return this.entityManager.merge(entity);		
	}

	@Override
	public void remove(Class<T> clazz, Long id) {
		T entity = findById(clazz, id);
		if (entity != null) {
			entityManager.remove(entity);
		} else {
			throw new NotFoundException();
		}
	}
		
}
