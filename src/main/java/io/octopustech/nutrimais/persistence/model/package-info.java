@XmlJavaTypeAdapters({
    @XmlJavaTypeAdapter(type = LocalDate.class, 
                        value = LocalDateAdapter.class),
    @XmlJavaTypeAdapter(type = LocalDateTime.class, 
    					value = LocalDateTimeAdapter.class)
})

package io.octopustech.nutrimais.persistence.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import io.octopustech.nutrimais.rest.util.LocalDateAdapter;
import io.octopustech.nutrimais.rest.util.LocalDateTimeAdapter;