package io.octopustech.nutrimais.service;

import java.util.List;

import javax.validation.ValidationException;
import javax.ws.rs.NotFoundException;

import io.octopustech.nutrimais.persistence.model.Message;

public interface MessageService {
	
	public List<Message> listAll();
	
	public Message add(Message message) throws ValidationException;
	
	public Message update(Message message) throws ValidationException;
	
	public void remove(Long messageId) throws NotFoundException, ValidationException;
}
