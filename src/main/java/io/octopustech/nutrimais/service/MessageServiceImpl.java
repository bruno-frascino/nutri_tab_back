package io.octopustech.nutrimais.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ValidationException;
import javax.ws.rs.NotFoundException;

import io.octopustech.nutrimais.persistence.dal.MessageDAO;
import io.octopustech.nutrimais.persistence.model.Message;
import io.octopustech.nutrimais.util.MessageBundle;
import io.octopustech.nutrimais.util.MessageKeys;
import io.octopustech.nutrimais.util.NutrimaisMessageBundle;

@RequestScoped
public class MessageServiceImpl implements MessageService{
	
	@Inject
	MessageDAO messageDAO;
	
	@Inject @MessageBundle
	NutrimaisMessageBundle messageBundle;
	
	@Override
	public List<Message> listAll() {
		return messageDAO.findAll(Message.class);
	}
	
	@Transactional
	@Override
	public Message add(Message message) throws ValidationException {
		
		validateMessage(message);
		message.setCreateDate(LocalDate.now());
		
		return this.messageDAO.save(message);
	}
	
	@Transactional
	@Override
	public Message update(Message message) throws ValidationException {
		
		validateMessage(message);
		message.setUpdateDate(LocalDateTime.now());
		
		return this.messageDAO.update(message);
	}
	
	@Transactional
	@Override
	public void remove(Long messageId) throws NotFoundException, ValidationException {
		
		if( messageId == null ){
			throw new ValidationException(this.messageBundle.getString(MessageKeys.ARG_INVALID));
		}
		
		this.messageDAO.remove(Message.class, messageId);
	}
	
	
	private void validateMessage(Message message) throws ValidationException {
		
		
		if( message == null ){
			throw new ValidationException(this.messageBundle.getString(MessageKeys.MSG_INVALID));
		}
		
		if( message.getMessage() == null || message.getMessage().trim().isEmpty() ){
			throw new ValidationException(this.messageBundle.getString(MessageKeys.MSG_EMPTY));
		}
	}
	
}
