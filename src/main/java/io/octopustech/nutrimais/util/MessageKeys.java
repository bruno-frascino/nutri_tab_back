package io.octopustech.nutrimais.util;

public enum MessageKeys {

	MSG_EMPTY("msg.error.empty"),
	MSG_INVALID("msg.error.invalid"),
	ARG_INVALID("arg.error.null");
	
	private String propertyKey;
	
	MessageKeys(String propertyKey ){
		this.propertyKey = propertyKey;
	}
	
	public String getPropertyKey(){
		return propertyKey;
	}
	
	@Override
    public String toString() {
        return propertyKey;
    }
}
