package io.octopustech.nutrimais.util;

import java.util.Locale;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class ResourceProducer {

	@Produces
	@PersistenceContext(unitName="nutrimaispu")
	private EntityManager em;
	

	@Produces 
	@MessageBundle
	private NutrimaisMessageBundle messageBundle = 
		new NutrimaisMessageBundle("messages", new Locale("pt", "BR"));
}
