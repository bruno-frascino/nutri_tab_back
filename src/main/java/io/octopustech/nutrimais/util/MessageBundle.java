package io.octopustech.nutrimais.util;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

//New Qualifier for the bundle so we can uniquely identify this bundle in a type safe manner. 
//We may want to produce and inject multiple resources that will all be of the same type (ResourceBundle) 
//so using a qualifier is a good idea.

@Qualifier
@Target({ TYPE, METHOD, PARAMETER, FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MessageBundle {
 
}