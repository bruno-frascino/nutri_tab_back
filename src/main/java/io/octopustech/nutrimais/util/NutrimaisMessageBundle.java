package io.octopustech.nutrimais.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class NutrimaisMessageBundle {

	private ResourceBundle resourceBundle;
	
	public NutrimaisMessageBundle(String bundleName, Locale locale ) {		
		this.resourceBundle  = ResourceBundle.getBundle(bundleName, locale);
	}
	
	public String getString(MessageKeys key){
		return this.resourceBundle.getString(key.getPropertyKey());
	}
	
	public String getString(MessageKeys key, Object... params){
		if(params != null && params.length > 0){
			return MessageFormat.format(this.getString(key), params);
		}
		return "";
	}
}
