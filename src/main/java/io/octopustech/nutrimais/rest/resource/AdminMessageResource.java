package io.octopustech.nutrimais.rest.resource;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.validation.ValidationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import io.octopustech.nutrimais.persistence.model.Message;
import io.octopustech.nutrimais.service.MessageServiceImpl;

@RequestScoped
@Path("/messages")
@Produces({"application/json"})
@Consumes({"application/json"})
public class AdminMessageResource {
	
	@Inject
	private MessageServiceImpl msgService;
	
	public AdminMessageResource() {}
	
	@GET
	public Response listAll(){
		return Response.status(Response.Status.OK).entity(this.msgService.listAll()).build();
	}
	
	@POST
	public Response add(Message message) throws ValidationException{
		Message msg = this.msgService.add(message);
		
		return Response.status(Status.CREATED)
				.entity(msg)
				.build();
	}
	
	@PUT
	public Response update(Message message) throws ValidationException{
		Message msg = this.msgService.update(message);
		
		return Response.status(Status.OK)
				.entity(msg)
				.build();
	}
	
	@DELETE
	@Path("/{messageId}")
	public Response remove(@PathParam("messageId") Long messageId) throws NotFoundException {	
		
		this.msgService.remove(messageId);
		
		return Response.status(Response.Status.NO_CONTENT).build();
	}

}
