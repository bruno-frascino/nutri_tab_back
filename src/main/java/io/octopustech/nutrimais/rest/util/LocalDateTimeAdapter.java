package io.octopustech.nutrimais.rest.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

	@Override
	public LocalDateTime unmarshal(String inputDateTime) throws Exception {
		return LocalDateTime.parse(inputDateTime, DateTimeFormatter.ISO_DATE_TIME);
	}

	@Override
	public String marshal(LocalDateTime dateTime) throws Exception {
		return DateTimeFormatter.ISO_DATE_TIME.format(dateTime);
	}

	
}
