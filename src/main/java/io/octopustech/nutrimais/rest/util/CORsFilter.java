package io.octopustech.nutrimais.rest.util;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.plugins.interceptors.CorsFilter;

@Provider
public class CORsFilter implements Feature, ContainerRequestFilter{

	@Override
	public boolean configure(FeatureContext context) {
		
		CorsFilter filter = new CorsFilter();
		filter.getAllowedOrigins().add("http://localhost:8100");
		filter.getAllowedOrigins().add("http://localhost:8180");
		filter.getAllowedOrigins().add("http://localhost:8080");
		filter.getAllowedOrigins().add("http://138.197.81.222:8080");
		filter.setAllowedMethods("OPTIONS, GET, POST, DELETE, PUT, PATCH");
		filter.setAllowedHeaders("origin, content-type, accept, authorization");
		filter.setAllowCredentials(true);
		//filter.setCorsMaxAge(1209600);
		
		context.register(filter);
		return true;
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// TODO Auto-generated method stub
		//do nothing now
	}

	
}
