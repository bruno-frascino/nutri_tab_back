### Welcome to Nutrimais Back End! 
 
Nutrimais Back End provides REST services to Nutrimais Front End app that has the purpose of helping patients to follow the recommended daily intake of calories recommend by the doctor.
 
- Version 0.1
 
### Architecture ###

Nutrimais Front End (Angular) <-> REST <-> Business <-> DAO <-> Database

 
### Dependencies ###
 
Maven version 3.3.9 project with dependencies on: 
 
* JDK 8
* Wildfly 10.1.0.Final - JEE 7 - CDI api 1.2
* PostgreSQL 9.6.4
* Hibernate Core 5.2.10.Final - JPA 2.1 - (Wildfly version updated)
* Resteasy 3.1.1.Final - (Wildfly version updated)
 
Database
 
* A datasource for PostgreSQL database should be created with the name NutriMaisDS
* Database schema generation is automated with the value DROP-AND-CREATE


### Contact ###

* Bruno Frascino - bfrascino80@gmail.com